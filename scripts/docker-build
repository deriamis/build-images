#!/bin/bash
set -eo pipefail

# If OS is specified ignore other ways to specify system
# Use DEBIAN if UBI is not specified
if [[ -n ${FROM_IMAGE} ]]; then
    unset DEBIAN OS UBI
elif [[ -n ${OS} ]]; then
    unset DEBIAN UBI
elif [[ -n ${UBI} ]]; then
    unset DEBIAN
else
    # Initialize it
    : "${DEBIAN:=bookworm}"
fi

# Initialize IMAGE_TAG if not already set
: "${IMAGE_TAG:=latest}"

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
source "$SCRIPT_DIR/lib/docker-build.sh"

function docker_build() {
    local image_name
    local docker_build_command

    mapfile -t docker_build_command < <(parse_env_vars)
    docker_build_command+=( "$@" )

    if [[ -n ${IMAGE_REPO} ]]; then
        image_name="${IMAGE_REPO}:${IMAGE_TAG}"
        docker_build_command+=("--cache-from=type=local,src=docker-cache")
        docker_build_command+=("--tag=${image_name}")
    else
        image_name="(dynamic image)"
    fi

    echo "Building ${image_name}"
    echo "Docker build command:"

    # Prefer using `docker buildx` as it is more flexible,
    # or use regular `docker build` if not available on older systems
    if docker buildx version &> /dev/null; then
        docker_build_command+=("--cache-to=type=local,dest=docker-cache,mode=max")
        docker_build_command+=("--push=true")
        docker_build_command+=("--provenance=false")
        run_command docker buildx build "${docker_build_command[@]}" .
    else
        docker_build_command+=("--build-arg TARGETARCH=${TARGETARCH}")
        run_command docker build "${docker_build_command[@]}" .
    fi
    printf "\n\nSUCCESS - Successfully built:\n\t%s\n" "${image_name}"
}

docker_build "$@"
