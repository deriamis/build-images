# Note: Check out https://wiki.bash-hackers.org/syntax/pe for documentation on
# various variable operations used in this script.

TARGETARCH=${ARCH:-amd64}

IMAGE_PROPERTIES=(IMAGE_REPO TARGETARCH)
OS_SELECTORS=(DEBIAN OS UBI FROM_IMAGE)
TOOL_SELECTORS=(GOLANG RUST NODE GIT LFS YARN DOCKER LLVM STATICCHECK GOLANGCI_LINT GOSEC)

function get_image_name() {
    echo "$(get_image_path):$(get_image_tag)"
}

function fail() {
    echo "$@" 1>&2
    exit 1
}

function build_arg() {
    local BUILD_ARG_VAR="$1"
    local BUILD_ARG_VALUE="$2"

    echo "--build-arg=${BUILD_ARG_VAR}=${BUILD_ARG_VALUE}"
}

function print_image_args() {
    local BASE_IMAGE
    local BUILD_OS
    local OS_VERSION

    case "$1:$2" in
        debian:*)
            BASE_IMAGE="$1:$2"
            BUILD_OS=debian
            OS_VERSION=$2
            ;;

        ubi:8.*)
            BASE_IMAGE="registry.access.redhat.com/ubi8/ubi:$2"
            BUILD_OS=ubi
            OS_VERSION=$2
            ;;
        custom:*)
            BASE_IMAGE="$2"
            ;;
        *)
            fail "Unknown image version $1:$2"
            ;;
    esac

    build_arg "BASE_IMAGE" "${BASE_IMAGE}"
    [[ -n ${BUILD_OS} ]] && build_arg "BUILD_OS" "${BUILD_OS}"
    [[ -n ${OS_VERSION} ]] && build_arg "OS_VERSION" "${OS_VERSION}"

    return 0
}

function print_golang_args() {
    declare -A GOLANG_DOWNLOAD_SHA256

    case "$1" in
        1.18)
            GOLANG_VERSION=1.18.10
            GOLANG_DOWNLOAD_SHA256[amd64]="5e05400e4c79ef5394424c0eff5b9141cb782da25f64f79d54c98af0a37f8d49"
            GOLANG_DOWNLOAD_SHA256[arm64]="160497c583d4c7cbc1661230e68b758d01f741cf4bece67e48edc4fdd40ed92d"
            ;;
        1.19)
            GOLANG_VERSION=1.19.12
            GOLANG_DOWNLOAD_SHA256[amd64]="48e4fcfb6abfdaa01aaf1429e43bdd49cea5e4687bd5f5b96df1e193fcfd3e7e"
            GOLANG_DOWNLOAD_SHA256[arm64]="18da7cf1ae5341e6ee120948221aff96df9145ce70f429276514ca7c67c929b1"
            ;;
        1.20)
            GOLANG_VERSION=1.20.7 # Update scripts/install-golang for FIPS if this version is bumped
            GOLANG_DOWNLOAD_SHA256[amd64]="f0a87f1bcae91c4b69f8dc2bc6d7e6bfcd7524fceec130af525058c0c17b1b44"
            GOLANG_DOWNLOAD_SHA256[arm64]="44781ae3b153c3b07651d93b6bc554e835a36e2d72a696281c1e4dad9efffe43"
            ;;
        1.21)
            GOLANG_VERSION=1.21.0 # Update scripts/install-golang for FIPS if this version is bumped
            GOLANG_DOWNLOAD_SHA256[amd64]="d0398903a16ba2232b389fb31032ddf57cac34efda306a0eebac34f0965a0742"
            GOLANG_DOWNLOAD_SHA256[arm64]="f3d4548edf9b22f26bbd49720350bbfe59d75b7090a1a2bff1afad8214febaf3"
            ;;
        *) fail "Unknown golang version $1" ;;
    esac

    build_arg "GOLANG_VERSION" "${GOLANG_VERSION}"
    build_arg "GOLANG_DOWNLOAD_SHA256" "${GOLANG_DOWNLOAD_SHA256[$TARGETARCH]}"

    return 0
}

# see https://www.kernel.org/pub/software/scm/git
function print_git_args() {
    case "$1" in
        2.33)
            GIT_VERSION=2.33.1
            GIT_DOWNLOAD_SHA256=02047f8dc8934d57ff5e02aadd8a2fe8e0bcf94a7158da375e48086cc46fce1d
            ;;
        2.36)
            GIT_VERSION=2.36.1
            GIT_DOWNLOAD_SHA256=37d936fd17c81aa9ddd3dba4e56e88a45fa534ad0ba946454e8ce818760c6a2c
            ;;
        2.42)
            GIT_VERSION=2.42.0
            GIT_DOWNLOAD_SHA256=34aedd54210d7216a55d642bbb4cfb22695b7610719a106bf0ddef4c82a8beed
            ;;
        *) fail "Unknown git version $1" ;;
    esac

    case "${GIT_VERSION}" in
        *.rc[0-9])
            GIT_DOWNLOAD_URL="https://www.kernel.org/pub/software/scm/git/testing/git-${GIT_VERSION}.tar.gz"
            ;;
        *)
            GIT_DOWNLOAD_URL="https://www.kernel.org/pub/software/scm/git/git-${GIT_VERSION}.tar.gz"
            ;;
    esac

    build_arg "GIT_VERSION" "${GIT_VERSION}"
    build_arg "GIT_DOWNLOAD_SHA256" "${GIT_DOWNLOAD_SHA256}"
    build_arg "GIT_DOWNLOAD_URL" "${GIT_DOWNLOAD_URL}"

    return 0
}

function parse_env_vars() {
    # We make use of 2 variables related to the build environment.
    # 1. `CUSTOM_DOCKER_IMAGE` - Defines the reference of image used as
    #                            base for building images. Follows a
    #                            valid docker image reference format
    #                              - "debibookworm"
    #                              - "registry.access.redhat.com/ubi8/ubi:8.6"
    # 2. `BUILD_OS`            - Used by installation scripts of various
    #                            components to detect the OS being worked
    #                            on. Follows regular docker image:tag
    #                            format - "debian:bookworm" or "ubi:8.6".

    for env_var in "${IMAGE_PROPERTIES[@]}"; do
        if [[ -n "${!env_var}" ]]; then
            case "${env_var}" in
                IMAGE_REPO) echo "--file=Dockerfile.${IMAGE_REPO##*/}" ;;
                TARGETARCH) echo "--platform=linux/${TARGETARCH}" ;;
                *) fail "Unknown image property ${env_var}" ;;
            esac
        fi
    done

    for env_var in "${OS_SELECTORS[@]}"; do
        if [[ -n "${!env_var}" ]]; then
            value="${!env_var}"
            case "${env_var}" in
                # explode debian:version into `print_image_args debian version`
                OS) print_image_args "${value%:*}" "${value##*:}" ;;
                DEBIAN) print_image_args debian "${value}" ;;
                UBI) print_image_args ubi "${value}" ;;
                FROM_IMAGE) print_image_args custom "${value}" ;;
                *) fail "Unknown OS selector ${env_var}" ;;
            esac
        fi
    done

    for env_var in "${TOOL_SELECTORS[@]}"; do
        if [[ -n "${!env_var}" ]]; then
            value="${!env_var}"
            case "${env_var}" in
                GOLANG) print_golang_args "${value}" ;;
                GIT) print_git_args "${value}" ;;
                DOCKER) build_arg "DOCKER_VERSION" "${value}" ;;
                LLVM) build_arg "LLVM_VERSION" "${value}" ;;
                STATICCHECK) build_arg "STATICCHECK_VERSION" "${value}" ;;
                GOLANGCI_LINT) build_arg "GOLANGCI_LINT_VERSION" "${value}" ;;
                GOSEC) build_arg "GOSEC_VERSION" "${value}" ;;
                *) fail "Unknown tool ${env_var}" ;;
            esac
        fi
    done
}

function run_command() {
    printf "\t%s\n" "$@"
    "$@"
}

